from flask import Flask, render_template, abort, jsonify, request, url_for, redirect
from model.database import load_db, save_db

app = Flask(__name__)


class Product:
    def __init__(self, id, name, active="false"):
        self.id = int(id)
        self.name = str(name)
        self.active = True if (active == "true") else False


def convert_input_to(obj, class_):
    obj = class_(**obj)
    return obj


@app.route("/")
def index():
    database = load_db()
    return render_template(
        "index.html",
        products=database["products"],
        author="TNT"
    )


@app.route("/products/<int:id>")
def product_detail(id):
    def filter_by_id(p): return p["id"] == id

    database = load_db()
    products_by_id = [
        product for product in database["products"] if filter_by_id(product)
    ]

    if len(products_by_id) == 0:
        # return "", 404
        abort(404)

    product = products_by_id[0]

    return render_template(
        "product.html",
        product=product
    )


@app.route("/products/add", methods=["GET", "POST"])
def add_product():
    if request.method == "POST":
        new_product = convert_input_to(request.form, Product)
        database = load_db()
        products = database["products"]

        exists = any(
            [product for product in products if product["id"] == new_product.id]
        )

        if exists:
            return render_template("add-product.html", message="Id already exists")

        products.append(vars(new_product))
        save_db(database)

        return redirect(url_for('product_detail', id=new_product.id))

    return render_template("add-product.html")


@app.route("/products/<int:product_id>/delete")
def delete_product(product_id):
    try:
        database = load_db()
        products = database["products"]
        deleted_product = [
            product for product in products if product["id"] == product_id
        ][0]

        products.remove(deleted_product)
        save_db(database)

        return redirect(url_for("index"))
    except IndexError:
        abort(404)


@app.route("/api/products/<int:id>")
def get_product_detail(id):
    def filter_by_id(p): return p["id"] == id

    database = load_db()
    products_by_id = [
        product for product in database["products"] if filter_by_id(product)
    ]

    if len(products_by_id) == 0:
        return "", 404

    product = products_by_id[0]
    return product


@app.route("/api/products")
def get_products():
    database = load_db()
    products = database["products"]
    return jsonify(products)
