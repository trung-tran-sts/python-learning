import json


def load_db():
    with open("data/database.json", encoding="utf-8") as file:
        return json.load(file)


def save_db(database):
    with open("data/database.json", mode="w", encoding="utf-8") as file:
        return json.dump(database, file)
