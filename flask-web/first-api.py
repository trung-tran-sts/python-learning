from flask import Flask, json
import datetime

app = Flask(__name__)
access_count = 0


@app.route("/")
def index():
    return {
        "authorName": "TNT",
        "age": 23
    }


@app.route("/date")
def get_date():
    now = datetime.datetime.now()
    return json.dumps(now)


@app.route("/format-string")
def format_string():
    return "This '{data}' is formatted at {now}".format(data="Some random string", now=datetime.datetime.now())


@app.route("/access-count")
def get_access_count():
    global access_count
    access_count += 1
    return f"This endpoint is accessed {access_count} times"
